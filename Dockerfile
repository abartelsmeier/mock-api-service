FROM node:latest

ADD . /mock-api-service

WORKDIR /mock-api-service

RUN npm install

CMD node index.js
