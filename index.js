const express = require('express')
const os = require('os')

const app = express()

app.get('/ok', function (req, res) {
  res.send(hostInfo())
})

app.get('/err', function (req, res) {
  res.status(500)
  res.send(hostInfo())
})

app.listen(80, function () {
  console.log('Example app listening on port 3000!')
})

function hostInfo() {
  var info = os.hostname() + '\n\n'
  info += JSON.stringify(os.networkInterfaces())
  return info
}

